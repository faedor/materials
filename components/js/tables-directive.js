'use strict';
app.directive('tablesBlock', function ($compile) {
    return {
        restrict: 'EA',
        scope: {},
        templateUrl: "html/tables-view.html",
        link: function (scope, element, attrs) {
            const
                matrixType = 'matrix',
                numberInputType = 'numberInput';

            let tabs = {
                elasticModules: 0,
                dielectricPenetration: 1,
                basicProps: 2,
                constDamping: 3
            };

            let tabsContent = {
                default: [
                    {
                        title: 'Упругие модули',
                        description: `Матрица упругих модулей 
                            x10<sup><small>10</small></sup>
                            Н/м<sup><small>2</small></sup>
                            `,
                        elements: [
                            {
                                type: matrixType,
                                dim: [6, 6]
                            },
                            {
                                type: numberInputType,
                                info:[
                                    {
                                        id: 'e',
                                        title: 'E'
                                    },
                                    {
                                        id: 'v',
                                        title: 'V'
                                    }
                                ]
                            }
                        ]
                    }, {
                        title: 'Диэлектрическая проницаемость',
                        description: 'Диэлектрическая проницаемость x10<sup><small>-12</small>Ф/м',
                        elements: [{
                                type: matrixType,
                                dim: [3, 3]
                            }]
                    }, {
                        title: 'Основные свойства',
                        elements: [{
                            type: numberInputType,
                            info: [{
                                id: 'density',
                                title: 'Плотность'
                            }]
                        }]
                    }, {
                        title: 'Постоянные демпфирования',
                        elements:[{
                            type: numberInputType,
                            info: [
                                {
                                    id: 'a',
                                    title: 'a'
                                },
                                {
                                    id: 'b',
                                    title: 'b'
                                }
                            ]
                        }]
                    }
                ]
            };

            // установка набора вкладок для отображения
            let tabsType = 'default';

            scope.openTab = (idx) => {
                let activeTabContent = element.find('.tab-content-active').removeClass('tab-content-active');
                let activeTabTitle = element.find('.tab-title-active').removeClass('tab-title-active');
                let tabTitle = element.find(`.tab-${idx}-title`);
                let tabContent = element.find(`.tab-${idx}-content`);
                tabTitle.toggleClass('tab-title-active');
                tabContent.toggleClass('tab-content-active');
                let buttons = element.find('.editor-buttons').show();
            };

            // создание контейнеров для содержимого вкладок
            let createContentWrappers = () => {
                let mainWrapper = element.find('.tabs-content-wrapper');
                let data = tabsContent[tabsType];
                for (let i in data){
                    mainWrapper.append(`
                        <div class='tab-content tab-${i}-content'></div>
                    `)
                }
                mainWrapper = $compile(mainWrapper)(scope);
            };

            // создание таблиц для вкладок с матрицами
            let addMatrixContent = (idx, data) => {
                let tabContent = element.find(`.tab-${idx}-content`);
                    let tableContent = '';
                    // цикл для создания строк
                    for (let j = 0; j< data.dim[0]; j++){
                        let tds = '';
                        for (let k = 0; k< data.dim[1]; k++){
                            tds += `
                                <td class='cell-${j}-${k}'>
                                    <input class='matrix-input matrix-input-${j}-${k}' type='text'>
                                </td>`;
                        }
                        tableContent += `<tr class='line-${j}'>${tds}</tr>`
                    }
                    let table = `<div class='content-element'>
                                    <table class='table-matrix table-matrix-${idx}'>${tableContent}</table>
                                 </div>`;
                    table = $compile(table)(scope);
                    tabContent.append(table);
            };

            let addInputContent = (idx, data) => {
                let tabContent = element.find(`.tab-${idx}-content`);
                let content = '';

                for (let i in data.info){

                    content += `
                                <div class='content-element'>
                                    <span class='input-title'>${data.info[i].title}</span>
                                    <input class='number-input number-input-${data.info[i].id}' type='number'>
                                    <br>
                                </div>`
                }
                content = $compile(content)(scope);
                tabContent.append(content);
            }; 
            
            //
            let createTabs = () => {
                let header = element.find('.tabs-header-wrapper');
                let content = element.find('.tabs-content-wrapper');
                let tabs = '';
                let contents = '';

                for (let i in tabsContent[tabsType]) {
                    let data = tabsContent[tabsType][i];
                    tabs += `
                            <div class='tab-title tab-${i}-title' ng-click='openTab(${i})'>
                                <div class='tab-title-text'>${data.title}</div>
                            </div>`;
                }
                tabs = $compile(tabs)(scope);
                header.append(tabs);
            };

            let createTabsContent = () => {
                let data = tabsContent[tabsType];
                for (let i in data){
                    let tabWrapper = element.find(`.tab-${i}-content`);
                    let descriptionBlock = `<div class='tab-description tab-${i}-description'>${data[i].description}`;
                    descriptionBlock = $compile(descriptionBlock)(scope);
                    tabWrapper.append(descriptionBlock);
                    for (let j in data[i].elements) {
                        let el = data[i].elements[j];
                        if (el.type === matrixType){
                            addMatrixContent(i, el)
                        }
                        if (el.type === numberInputType){
                            addInputContent(i, el)
                        }
                    }
                }
            };

            scope.saveData = () => {
                let data = collectAllData();
                console.log(data)
            };

            let collectAllData = () => {
                return {
                    elasticModules: collectElasticData(),
                    dielectricPenetration: collectDielectricData(),
                    basicProps: collectBasicData(),
                    constDamping: collectDampingData()
                };
            };

            let collectElasticData = () => {
                // TODO: возможно, нужно будет определять размерность матрицы, на данный момент используется 6x6
                let dim = 6;
                let result = {
                    matrix: [],
                    e: null,
                    v: null
                };

                let idx = tabs.elasticModules;
                let tab = $(`.tab-${idx}-content`);
                for (let i = 0; i < dim; i++){
                    let row = [];
                    for (let j = 0; j < dim; j++) {
                        row.push(tab.find(`.matrix-input-${i}-${j}`)[0].value)
                    }
                    result.matrix.push(row);
                }
                result.e = tab.find('.number-input-e')[0].value;
                result.v = tab.find('.number-input-v')[0].value;

                return result
            };

            let collectDielectricData = () => {
                let idx = tabs.dielectricPenetration;
                let tab = $(`.tab-${idx}-content`);
                // TODO: возможно, нужно будет определять размерность матрицы, на данный момент используется 3x3
                let dim = 3;
                let result = [];
                for (let i =0; i < dim; i++){
                    let row = [];
                    for (let j =0; j < dim; j++) {
                        row.push(tab.find(`.matrix-input-${i}-${j}`)[0].value)
                    }
                    result.push(row);
                }
                return result;

            };

            let collectBasicData = () => {
                let idx = tabs.basicProps;
                let tab = $(`.tab-${idx}-content`);
                return {
                    density: tab.find('.number-input-density')[0].value
                }
            };

            let collectDampingData = () => {
                let idx = tabs.constDamping;
                let tab = $(`.tab-${idx}-content`);
                return {
                    a: tab.find('.number-input-a')[0].value,
                    b: tab.find('.number-input-b')[0].value
                }
            };



            createTabs();
            createContentWrappers();
            createTabsContent();
            scope.openTab(0);
        }
    }
});