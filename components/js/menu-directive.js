'use strict';
app.directive('materialsMenu', ($compile) => {
        return {
            restrict: 'EA',
            scope: {},
            templateUrl: 'html/menu-view.html',
            link: (scope, element, attrs) => {

                /*
                * Соответствие типов вкладок и идентификаторов элементов
                * TODO: вынести в отдельный модуль, т.к. используется в 'tables-directive'
                * TODO: подумать над тем, чтобы использовать в идентификаторах элементов сразу название вкладки
                * */
                let tabs = {
                    elasticModules: 0,
                    dielectricPenetration: 1,
                    basicProps: 2,
                    constDamping: 3
                };

                let menuData = [
                    {
                        id: 1,
                        name: "Изотропный",
                        materials: [
                            {id: 1, name: "Материал 1"},
                            {id: 2, name: "Материал 2"},
                            {id: 3, name: "Материал 3"},
                            {id: 4, name: "Материал 4"},
                            {id: 5, name: "Материал 5"},
                            {id: 11, name: "Материал 6"},
                            {id: 12, name: "Материал 7"},
                            {id: 13, name: "Материал 8"},
                            {id: 14, name: "Материал 9"},
                            {id: 15, name: "Материал 10"},
                            {id: 16, name: "Материал 11"},
                            {id: 17, name: "Материал 12"},
                            {id: 18, name: "Материал 13"},
                            {id: 19, name: "Материал 14"},
                            {id: 20, name: "Материал 15"}
                        ]
                    },
                    {
                        id: 2,
                        name: "Анизотропный",
                        materials: [
                            {id: 6, name: "Материал 1"},
                            {id: 7, name: "Материал 2"},
                            {id: 8, name: "Материал 3"},
                            {id: 9, name: "Материал 4"},
                            {id: 10, name: "Материал 5"}
                        ]
                    }

                ];

                let getMaterialData = (materialId) => {
                    // TODO: обращаемся к серверу для получения данных материала
                    let materialData = {
                        elasticModules:
                            {
                                matrix: [
                                    [1,2,3,4,5,6],
                                    [11,12,13,14,15,16],
                                    [21,22,3,4,6,9],
                                    [0,4,0,1,2,3],
                                    [6,7,8,9,0,3],
                                    [2,3,4,5,6,7]
                                ],
                                e: 21,
                                v: 22
                            },
                        dielectricPenetration: [
                            [1,2,3],
                            [4,5,6],
                            [7,8,9]
                        ],
                        basicProps: {
                            density: 3.56
                        },
                        constDamping: {
                            a: 12,
                            b: 34
                        }
                    };
                    return materialData;
                };

                scope.activeSubmenuId = null;

                /*
                * @description обработчик открывания-закрывания подменю
                * @param idToActive - идентификатор подменю, которое необходимо открыть
                */
                scope.collapseHandler = (idToActive) =>{
                    let current_items = element.find(`.submenu-${idToActive}-items`)
                        .toggleClass('submenu-active');
                    let separators = element.find(`.submenu-${idToActive}`).find('.separator');
                    separators.toggleClass('separator-active');
                };

                scope.addMaterial = (typeId) => {
                    let active = element.find('.selected-material');
                    active.removeClass('selected-material');
                    let editor = $('.editor-wrapper');
                    let inputName = `<input type="text" class="custom-input" placeholder="Введите название">`;
                    let descr = '<div class="editor-title">Добавление материала</div>';
                    editor.find('.editor-title').html(descr+inputName);
                    editor.show();
                };

                scope.selectMaterial = (id) => {
                    let active = element.find('.selected-material');
                    let current = element.find(`.submenu-item-${id}`);
                    let editor = $('.editor-wrapper');
                    if (current.hasClass('selected-material')){
                        //скрыть редактор
                        editor.hide();
                        current.removeClass('selected-material')
                    } else {
                        //открыть редактор
                        active.removeClass('selected-material');
                        current.addClass('selected-material');
                        let materialName = current.find('.submenu-item-text').html();
                        editor.find('.editor-title').html(materialName);
                        editor.show();
                        fillTables(id)
                    }
                };

                /*
                * Заполнение таблиц данными выбранного материала
                * */
                let fillTables = (materialId) => {
                    let materialData = getMaterialData(materialId);
                    fillElasticTab(materialData.elasticModules);
                    fillDielectricTab(materialData.dielectricPenetration);
                    fillBasicTab(materialData.basicProps);
                    fillConstDampingTab(materialData.constDamping)
                };

                let fillElasticTab = (data) => {
                    let matrixData = data.matrix;
                    let idx = tabs.elasticModules;
                    let tab = $(`.tab-${idx}-content`);
                    for (let i in matrixData){
                        for (let j in matrixData[i]) {
                            tab.find(`.matrix-input-${i}-${j}`)[0].value = matrixData[i][j]
                        }
                    }
                    tab.find('.number-input-e')[0].value = data.e;
                    tab.find('.number-input-v')[0].value = data.v
                };

                let fillDielectricTab = (data) => {
                    let idx = tabs.dielectricPenetration;
                    let tab = $(`.tab-${idx}-content`);
                    for (let i in data){
                        for (let j in data[i]) {
                            tab.find(`.matrix-input-${i}-${j}`)[0].value = data[i][j]
                        }
                    }
                };

                let fillBasicTab = (data) => {
                    let idx = tabs.basicProps;
                    let tab = $(`.tab-${idx}-content`);
                    tab.find('.number-input-density')[0].value = data.density;
                };

                let fillConstDampingTab = (data) => {
                    let idx = tabs.constDamping;
                    let tab = $(`.tab-${idx}-content`);
                    tab.find('.number-input-a')[0].value = data.a;
                    tab.find('.number-input-b')[0].value = data.b
                };

                /*
                @description создание меню
                 */
                let createMenu = () => {
                    // основной блок меню
                    let menuWrapper = element.find('.materials-menu');

                    // формирование меню
                    let menuResult = '';
                    for (let i in menuData){
                        // добавление подменю для каждого типа материала
                        let submenu = {};
                        let submenuId = menuData[i].id;
                        // заголовок подменю
                        submenu.header =
                            `<div class='separator separator-inactive'></div>
                                <div class='submenu-header' ng-click='collapseHandler(${submenuId})'>
                                    <div class='submenu-header-text'>${menuData[i].name}</div>
                                    <div class='submenu-header-icons' ng-click='$event.stopPropagation();'>
                                        <div class='search-icon glyphicon glyphicon-plus-sign' 
                                        ng-click='addMaterial(${submenuId})'></div>
                                    </div>
                                </div>
                            <div class='separator separator-inactive'></div>`;

                        // формирование списка элементов меню
                        submenu.body = '';
                        for (let material of menuData[i].materials){
                            submenu.body +=
                                `<div class='submenu-item submenu-item-${material.id}' ng-click='selectMaterial(${material.id})'>
                                    <div class='submenu-item-text'>${material.name}</div>
                                    <div class='submenu-item-icons'></div>
                                </div>`;
                        }

                        // добавление подменю в основное
                        menuResult +=
                            `<div class='submenu submenu-${submenuId}'>
                                ${submenu.header} 
                                <div class='items-wrapper'>
                                    <div class='submenu-items submenu-${submenuId}-items'>
                                        ${submenu.body}
                                    </div>
                                </div>
                            </div>`;
                    }
                    menuResult = $compile(menuResult)(scope);
                    menuWrapper.append(menuResult);
                };
                createMenu();
            }
    }});